package com.webapp.croptrack;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CroptrackApplication {

	public static void main(String[] args) {
		SpringApplication.run(CroptrackApplication.class, args);
	}

}
